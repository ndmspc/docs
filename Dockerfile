FROM fedora:40
RUN dnf install python3-pip git findutils nodejs ImageMagick pandoc weasyprint texlive-latex texlive-xetex texlive-lm-math texlive-metafont -y
RUN pip install --no-cache-dir mkdocs-material mkdocs-bibtex mkpdfs-mkdocs mkdocs-mermaid2-plugin mkdocs-macros-plugin mkdocs-glightbox mkdocs-git-revision-date-localized-plugin
CMD ["/bin/bash"]
