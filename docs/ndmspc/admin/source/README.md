# Admin guide

This admin guide is intended to use by system administrators, who wants to setup all required services of this project or setup Kubernetes cluster with our project. The guide will discuss step-by-step process of installation all of the required packages, setting up services and final testing of our project when deployed.

!!! warning

    Safari is not supported.


## Setup on Kubernetes as quickstart

### Prerequisites

- [Kubernetes](https://kubernetes.io)


One of the options on how to setup our project is to set it up on Kubernetes cluster.

```bash
KIND=0 bash <(curl -s https://gitlab.com/ndmspc/user/-/raw/main/k8s/ndmspc/install.sh)
```

Installation is fully finished, when ndmspc pods are in `running` state. To find out pod state, user can execute:

```bash
kubectl get pods
```

User must wait until all pods have a state of `Running`. After that, user can open his web browser and go to `http://localhost` or `http://127.0.0.1`. If everything went well, he should see our web application ready to use. Example of the web application is below.

![](images/main.png "main page")

When user writes `kubectl get pods` command, he can see all the pods that are currently available. These pods are basically running Docker images (*containers*). This means that this can be used also via Docker itself.

## Setup self-hosted via Docker container

!!! warning
    Since we are using `--net=host`, Linux is required for this type of installation.

### Prerequisites

- [Docker](https://docs.docker.com/get-docker/) or [Podman](https://podman.io/getting-started/)

If user wants to run the project via Docker container, he needs several Docker images. First, he needs to pull and run SALSA Docker image. This can be done with this command:

```
docker run -it --rm --net=host registry.gitlab.com/ndmspc/salsa
```

Next step is to pull and run Zmq2Ws Docker image. There is also an option to include custom configuration file in YAML format. It can be mounted as a volume to `/app/config.yml` inside of the container. Example configuration file is available on [GitLab](https://gitlab.com/ndmspc/zmq2ws/-/raw/main/config.yml)

```
docker run --net=host -v config.yml:/app/config.yml registry.gitlab.com/ndmspc/zmq2ws
```

At this moment, user can submit jobs simply using SALSA Docker image and `salsa-feeder` command that is included in it. For example, if user wants to submit a job with command `sleep 1` executed 100 times. User can use the following `docker run` command to submit it:

```
docker run -it --rm --net=host --entrypoint salsa-feeder registry.gitlab.com/ndmspc/salsa -u tcp://localhost:41000 -t "sleep 1:100"
```

After running Zmq2Ws Docker image, user needs to run also the web application. For the sake of this guide, we will be using example project inside of the `react-ndmspc` project. We have prepared also Docker image for it, so user can pull and run the image with:

```
docker run --net=host -d registry.gitlab.com/ndmspc/react-ndmspc
```

This will spin up an NGINX HTTP server that serves the content of the `example` directory inside of the `react-ndmspc` project. Again, after successful running of this Docker image, user can open his web browser and go to `http://localhost` or `http://127.0.0.1`. If everything went well, he should see our web application ready to use. Example of the web application is below.

![](images/main.png "main page")

The last part of the project is the executor API. Again, we prepared a Docker image for it, so user needs only to pull it and run it. When user executes the following command, our executor API will be running on port `3000` on the host machine.

```
docker run --net=host -d registry.gitlab.com/ndmspc/api
```

We don't need to specify port forwarding here, since we are using `--net=host` flag, that instructs Docker to use host's network card. After successful run of all four Docker images, user can use our project with no restrictions.

## Setup self-hosted on CentOS 8

### Prerequisites

- [Python](https://www.python.org/downloads/) (version 3.9+)
- [Node.js](https://nodejs.org/en/) (LTS version)

If user is running CentOS 8 as his operating system, he can also use our RPM packages to install and run it. For all of our RPM packages we have a COPR repository, that is located on [Fedora COPR](https://copr.fedorainfracloud.org/coprs/ndmspc/stable/).

To be able to use our repository, user needs to install COPR plugin, PowerTools repository, enable them and after that he will be able to install the packages. To accomplish this, user can execute these commands:

```
dnf install epel-release 'dnf-command(config-manager)' 'dnf-command(copr)' -y
dnf config-manager --set-enabled powertools
dnf copr enable ndmspc/stable -y
dnf install salsa zmq2ws -y
```

After executing, user needs to setup required services for SALSA and Zmq2Ws to start on boot and also start it right away. To start it on boot, we can use `systemctl enable` command and to start it right now, we can also provide `--now` flag to this command. User can execute these commands to accomplish it:

```
systemctl enable salsa@wk --now
systemctl enable salsa@rdr --now
systemctl enable salsa-discovery --now
systemctl enable zmq2ws --now
```

After starting those services, user cannot access the cluster. To be able to reach it, user needs to permit port `40000` via UDP protocol. This can be done via `firewall-cmd` command as showned here:

```
firewall-cmd --add-port=40000/udp --permanent
firewall-cmd --reload
```

Next step is to install and use the executor API. We uploaded our `ndmspc-api` Python package to PyPi. To use it, user needs to have also `uvicorn` package, which is an ASGI web server for Python. User can install those using the following command:

```
pip install uvicorn ndmspc-api==0.0.2
```

After installation, the package is installed in `/usr/local/lib/python3.9/site-packages/` directory (depending on Python version, which in this case is Python 3.9). To run it, user can execute the following command (Python 3.9 executable is `python3.9`):

```
python3.9 /usr/local/lib/python3.9/site-packages/executor/ndmspc-executor.py
```

Now, user can create a HTTP request to `http://localhost:41001` and it should respond with `{"status":"ok"}` JSON response.

Last step is to install and run web application. This can be cloned from [GitLab](https://gitlab.com/ndmspc/react-ndmspc.git). Since it is a Node.js application, user can simply install required dependencies with `npm` command. To install dependencies and run the web application, user can execute following commands:

```
git clone https://gitlab.com/ndmspc/react-ndmspc.git
cd react-ndmspc
npm run reset
```

After that, user can access the web application by going to `http://localhost:3000` or `http://127.0.0.1:3000`. If everything went well, he should see our web application ready to use. Example of the web application is below.

![](images/main.png "main page")


## Setup self-hosted from source

### Prerequisites

- [Make](https://www.gnu.org/software/make/)
- [CMake](https://cmake.org)
- [Python](https://www.python.org/downloads/) (version 3.9+)
- [Node.js](https://nodejs.org/en/) (LTS version)

Last option mentioned in this guide is to build the project from source. To speed up the process, there is an installation shell script, that will setup everything from one place. User can use it to install and set up SALSA batching system. To install it, user can execute the following commands:

```
git clone https://gitlab.com/ndmspc/salsa.git
cd salsa
scripts/make.sh
```

After installation of SALSA, user also need to install and run Zmq2Ws broker. This can be accomplished with `npm` command:

```
git clone https://gitlab.com/ndmspc/zmq2ws.git
cd zmq2ws
npm i
npm start
```

Zmq2Ws will be running on port `8442` by default. After having both SALSA and Zmq2Ws running, user needs to run executor API. Since this is using Python's package FastAPI, user needs to use Python `pip` and `python` commands (in this case `pip3.9` and `python3.9` since we are using Python 3.9). To run executor API, user can execute these commands:

```
git clone https://gitlab.com/ndmspc/api.git
cd api/executor/executor
pip3.9 install -r requirements.txt
python3.9 server.py
```

Now the executor API will listen for HTTP requests on port `41001`. Last step is to install and run web application. This is written in React, which is Javascript library, so it uses Node.js to run it. User needs to use `npm` commands. To run web application, user can enter following commands:

```
git clone https://gitlab.com/ndmspc/react-ndmspc.git
cd react-ndmspc
npm run reset
```

After that, all four services are up and running, so user can access the web application by going to `http://localhost:3000` or `http://127.0.0.1:3000`. If everything went well, he should see our web application ready to use. Example of the web application is below.

![](images/main.png "main page")