# Local keycloak

## Start keycloak as container

```
podman run -d --name keycloak -p 8080:8080 -e KEYCLOAK_ADMIN=admin -e KEYCLOAK_ADMIN_PASSWORD=admin quay.io/keycloak/keycloak:24.0.2 start-dev
```

## Init ndmspc

Login as admin
```
podman exec -it keycloak /opt/keycloak/bin/kcadm.sh config credentials --server http://localhost:8080 --realm master --user admin --password admin
```
Create `ndmspc` realm
```
podman exec -it keycloak /opt/keycloak/bin/kcadm.sh create realms -s realm=ndmspc -s enabled=true
```
Create user `ndmspc`
```
podman exec -it keycloak /opt/keycloak/bin/kcadm.sh create users -r ndmspc -s username=ndmspc -s email=ndmspc@gmail.com -s firstName=NDM -s lastName=SPC -s emailVerified=true -s enabled=true -o --fields id,username
```
Set password
```
podman exec -it keycloak /opt/keycloak/bin/kcadm.sh  set-password -r ndmspc --username ndmspc --new-password 12345
```

Create `ndmspc-web-devel`
```
podman exec -it keycloak /opt/keycloak/bin/kcadm.sh create clients -r ndmspc -s enabled=true -s clientId=ndmspc-web-devel -s name='NDMSPC web devel' -s publicClient=true -s 'redirectUris=["http://localhost:5173/*"]' -s origin=["http://localhost:5173"]
```

## Start and stop container

To start container do
```
podman start keycloak
```

To stop container do
```
podman stop keycloak
```