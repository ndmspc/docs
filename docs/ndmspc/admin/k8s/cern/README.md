# NDMSPC k8s at CERN

## Start k8s cluster

!!! note
    In the end of this instruction, ndmspc web page will be accessible at [https://{==mvala-ndmspc==}.cern.ch](https://mvala-ndmspc.cern.ch). 
    
    * Please substitute hostname {==mvala-ndmspc==} used in this tutorial (highlighted in yellow) by other unique value like `myusername`. 
    * This string will be used to generate DNS entry in `cern.ch` domain (eg. `myusername.cern.ch`)


Login to `lxplus.cern.ch`

```
ssh lxplus.cern.ch
```

Install k8s cluster. More info is at [https://kubernetes.docs.cern.ch/docs/getting-started/](https://kubernetes.docs.cern.ch/docs/getting-started/)

```
$ openstack coe cluster create ndmspc --keypair robotkey --cluster-template kubernetes-1.29.2-1 --node-count 3
```

Check if cluster is ready by checking `status` to be `CREATE_COMPLETE`
```
$ openstack coe cluster list
+--------------------------------------+--------+----------+------------+--------------+-----------------+---------------+
| uuid                                 | name   | keypair  | node_count | master_count | status          | health_status |
+--------------------------------------+--------+----------+------------+--------------+-----------------+---------------+
| bed9f40a-91a3-45f8-90d1-489e24a8b367 | ndmspc | robotkey |          3 |            1 | CREATE_COMPLETE | None          |
+--------------------------------------+--------+----------+------------+--------------+-----------------+---------------+
```

Now save `kubectl` configuration
```
openstack coe cluster config ndmspc > mvala-ndmspc-k8s-env.sh
source mvala-ndmspc-k8s-env.sh
```

See if cluster is running
```
$ kubectl get all 
NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
service/kubernetes   ClusterIP   10.254.0.1   <none>        443/TCP   130m

```


## Setup ingress
Get list of nodes
```
$ kubectl get nodes
NAME                           STATUS   ROLES    AGE   VERSION
ndmspc-2a6tqjfd4agz-master-0   Ready    master   6m43s   v1.29.2
ndmspc-2a6tqjfd4agz-node-0     Ready    <none>   3m54s   v1.29.2
ndmspc-2a6tqjfd4agz-node-1     Ready    <none>   3m58s   v1.29.2
ndmspc-2a6tqjfd4agz-node-2     Ready    <none>   3m53s   v1.29.2
```

Add `ingress` role to these nodes (without master)
```
$ kubectl label node ndmspc-2a6tqjfd4agz-node-0 role=ingress
node/ndmspc-2a6tqjfd4agz-node-0 labeled
$ kubectl label node ndmspc-2a6tqjfd4agz-node-1 role=ingress
node/ndmspc-2a6tqjfd4agz-node-1 labeled
$ kubectl label node ndmspc-2a6tqjfd4agz-node-2 role=ingress
node/ndmspc-2a6tqjfd4agz-node-2 labeled
```
Adding dns entry for {==mvala-ndmspc==} (`mvala-ndmspc.cern.ch`) to all nodes that were marked in `role=ingress` in the previous step
```
$ openstack server set --property landb-alias={==mvala-ndmspc==}--load-1- ndmspc-2a6tqjfd4agz-node-0
$ openstack server set --property landb-alias={==mvala-ndmspc==}--load-2- ndmspc-2a6tqjfd4agz-node-1
$ openstack server set --property landb-alias={==mvala-ndmspc==}--load-3- ndmspc-2a6tqjfd4agz-node-2
```

```
openssl req -newkey rsa:2048 -x509 -sha256 -days 365 -nodes -out tls.crt -keyout tls.key -subj "/CN={==mvala-ndmspc==}.cern.ch/emailAddress=martin.vala@cern.ch"
```

```
$ kubectl create secret tls ndmspc-sample-tls-cert --key=tls.key --cert=tls.crt
```
## Install OLM
```
$ curl -sL https://github.com/operator-framework/operator-lifecycle-manager/releases/download/v0.27.0/install.sh | bash -s v0.27.0
customresourcedefinition.apiextensions.k8s.io/catalogsources.operators.coreos.com created
customresourcedefinition.apiextensions.k8s.io/clusterserviceversions.operators.coreos.com created
customresourcedefinition.apiextensions.k8s.io/installplans.operators.coreos.com created
customresourcedefinition.apiextensions.k8s.io/olmconfigs.operators.coreos.com created
customresourcedefinition.apiextensions.k8s.io/operatorconditions.operators.coreos.com created
...
...
...
catalogsource.operators.coreos.com/operatorhubio-catalog created
Waiting for deployment "olm-operator" rollout to finish: 0 of 1 updated replicas are available...
deployment "olm-operator" successfully rolled out
deployment "catalog-operator" successfully rolled out
Package server phase: Installing
Package server phase: Succeeded
deployment "packageserver" successfully rolled out

```
## Install NDMSPC operator
```
cat <<EOF | kubectl apply -f -
apiVersion: operators.coreos.com/v1alpha1
kind: Subscription
metadata:
  name: my-ndmspc-operator
  namespace: operators
spec:
  channel: alpha
  name: ndmspc-operator
  source: operatorhubio-catalog
  sourceNamespace: olm
  installPlanApproval: Automatic
EOF
```

```
$ kubectl apply -f k8s/ndmspc-operator.yaml 
subscription.operators.coreos.com/my-ndmspc-operator created
$ kubectl get csv
No resources found in default namespace.
$ kubectl get csv
NAME                      DISPLAY           VERSION   REPLACES                  PHASE
ndmspc-operator.v0.11.6   NdmSpc operator   0.11.6    ndmspc-operator.v0.11.5   Pending
```
Check if the operator is installed (PHASE collon has to be Succeeded)

```
$ kubectl get csv
NAME                      DISPLAY           VERSION   REPLACES                  PHASE
ndmspc-operator.v0.11.6   NdmSpc operator   0.11.6    ndmspc-operator.v0.11.5   Succeeded
```

## Create NDMSPC cluster

```
cat <<EOF | kubectl apply -f -
apiVersion: apps.ndmspc.io/v1alpha1
kind: NdmSpcConfig
metadata:
  labels:
    app.kubernetes.io/name: ndmspcconfig
    app.kubernetes.io/instance: ndmspcconfig-sample
    app.kubernetes.io/part-of: ndmspc-operator
    app.kubernetes.io/managed-by: kustomize
    app.kubernetes.io/created-by: ndmspc-operator
  name: ndmspc-sample
spec:
  worker:
    resources:
      requests:
        cpu: "100m"
        memory: "1Gi"
      limits:
        memory: "2Gi"
        cpu: "200m"
  workers: 2
  workerslots: 1
  ingressenabled: true
  ingressnostname: {==mvala-ndmspc==}
  ingresshostdomain: cern.ch
  ingressclasstype: traefik
  ingressssltraefik: true
EOF

```

Check if the NDMSPC cluster is running

```
$ kubectl get all,ingress,csv
NAME                                            READY   STATUS    RESTARTS   AGE
pod/ndmspc-sample-executor-7bcbb64b5-sx6fj      1/1     Running   0          82s
pod/ndmspc-sample-ndmspc-web-74fd58cf6b-8dmwj   1/1     Running   0          66s
pod/ndmspc-sample-ndmvr-65d9bdbcb9-qz64l        1/1     Running   0          85s
pod/ndmspc-sample-slsd-5cc96ffb9c-b2plt         1/1     Running   0          74s
pod/ndmspc-sample-slsr-5957c4f4cb-4wxq4         1/1     Running   0          72s
pod/ndmspc-sample-slsw-7d8c7f8cf9-4b66d         1/1     Running   0          70s
pod/ndmspc-sample-slsw-7d8c7f8cf9-cv55d         1/1     Running   0          70s
pod/ndmspc-sample-zmq2ws-67459675fd-vqpld       1/1     Running   0          90s

NAME                                          TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)     AGE
service/kubernetes                            ClusterIP   10.254.0.1       <none>        443/TCP     12m
service/ndmspc-sample-executor-service        ClusterIP   10.254.45.64     <none>        41001/TCP   83s
service/ndmspc-sample-ndmspc-web-service      ClusterIP   10.254.197.160   <none>        10000/TCP   68s
service/ndmspc-sample-ndmvr-ws-service        ClusterIP   10.254.195.26    <none>        8443/TCP    87s
service/ndmspc-sample-sls-discovery-service   ClusterIP   None             <none>        40000/TCP   76s
service/ndmspc-sample-sls-mon-service         ClusterIP   10.254.200.92    <none>        5001/TCP    80s
service/ndmspc-sample-sls-submitter-service   ClusterIP   10.254.146.53    <none>        41000/TCP   78s
service/ndmspc-sample-zmq2ws-mgr-service      ClusterIP   10.254.159.94    <none>        10000/TCP   92s
service/ndmspc-sample-zmq2ws-ws-service       ClusterIP   10.254.76.95     <none>        8442/TCP    94s

NAME                                       READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/ndmspc-sample-executor     1/1     1            1           82s
deployment.apps/ndmspc-sample-ndmspc-web   1/1     1            1           66s
deployment.apps/ndmspc-sample-ndmvr        1/1     1            1           85s
deployment.apps/ndmspc-sample-slsd         1/1     1            1           74s
deployment.apps/ndmspc-sample-slsr         1/1     1            1           72s
deployment.apps/ndmspc-sample-slsw         2/2     2            2           70s
deployment.apps/ndmspc-sample-zmq2ws       1/1     1            1           90s

NAME                                                  DESIRED   CURRENT   READY   AGE
replicaset.apps/ndmspc-sample-executor-7bcbb64b5      1         1         1       82s
replicaset.apps/ndmspc-sample-ndmspc-web-74fd58cf6b   1         1         1       66s
replicaset.apps/ndmspc-sample-ndmvr-65d9bdbcb9        1         1         1       85s
replicaset.apps/ndmspc-sample-slsd-5cc96ffb9c         1         1         1       74s
replicaset.apps/ndmspc-sample-slsr-5957c4f4cb         1         1         1       72s
replicaset.apps/ndmspc-sample-slsw-7d8c7f8cf9         2         2         2       70s
replicaset.apps/ndmspc-sample-zmq2ws-67459675fd       1         1         1       90s

NAME                                                              CLASS    HOSTS                  ADDRESS   PORTS     AGE
ingress.networking.k8s.io/ndmspc-sample-ingress-ndmspc            <none>   {==mvala-ndmspc==}.cern.ch             80, 443   64s
ingress.networking.k8s.io/ndmspc-sample-ingress-redirect-ndmspc   <none>   {==mvala-ndmspc==}.cern.ch             80        62s

NAME                                                                 DISPLAY           VERSION   REPLACES                  PHASE
clusterserviceversion.operators.coreos.com/ndmspc-operator.v0.11.6   NdmSpc operator   0.11.6    ndmspc-operator.v0.11.5   Succeeded
```

Check if dns entry `{==mvala-ndmspc==}.cern.ch` is created. One might wait around 15 minutes for changes to be applied.
```
$ host {==mvala-ndmspc==}.cern.ch
{==mvala-ndmspc==}.cern.ch has address 188.185.90.161
{==mvala-ndmspc==}.cern.ch has address 188.185.127.143
{==mvala-ndmspc==}.cern.ch has address 188.185.90.245
```

Now one can access [https://{==mvala-ndmspc==}.cern.ch](https://mvala-ndmspc.cern.ch)