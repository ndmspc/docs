# NDMSPC local k8s cluster

## Installation k8s cluster with ndmspc operator
The following script will install k8s cluster with all depencenies to deploy NDMSPC cluster
```
$ bash <(curl -s https://gitlab.com/ndmspc/ndmspc-operator/-/raw/main/scripts/install.sh)
```

??? example "The output of the script"

    ```
    $ bash <(curl -s https://gitlab.com/ndmspc/ndmspc-operator/-/raw/main/scripts/install.sh)
    🔥  Deleting "minikube" in podman ...
    🔥  Deleting container "minikube" ...
    🔥  Removing /var/home/mvala/.minikube/machines/minikube ...
    💀  Removed all traces of the "minikube" cluster.
    😄  minikube v1.32.0 on Fedora 39
    ✨  Using the podman driver based on user configuration
    📌  Using Podman driver with root privileges
    👍  Starting control plane node minikube in cluster minikube
    🚜  Pulling base image ...
    E0321 09:25:54.704112   57431 cache.go:189] Error downloading kic artifacts:  not yet implemented, see issue #8426
    🔥  Creating podman container (CPUs=2, Memory=3800MB) ...
    🐳  Preparing Kubernetes v1.28.3 on Docker 24.0.7 ...
        ▪ Generating certificates and keys ...
        ▪ Booting up control plane ...
        ▪ Configuring RBAC rules ...
    🔗  Configuring bridge CNI (Container Networking Interface) ...
    🔎  Verifying Kubernetes components...
        ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
    🌟  Enabled addons: storage-provisioner, default-storageclass
    🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
    💡  ingress is an addon maintained by Kubernetes. For any concerns contact minikube on GitHub.
    You can view the list of minikube maintainers at: https://github.com/kubernetes/minikube/blob/master/OWNERS
        ▪ Using image registry.k8s.io/ingress-nginx/kube-webhook-certgen:v20231011-8b53cabe0
        ▪ Using image registry.k8s.io/ingress-nginx/controller:v1.9.4
        ▪ Using image registry.k8s.io/ingress-nginx/kube-webhook-certgen:v20231011-8b53cabe0
    🔎  Verifying ingress addon...
    🌟  The 'ingress' addon is enabled
    ❗  metallb is a 3rd party addon and is not maintained or verified by minikube maintainers, enable at your own risk.
    ❗  metallb does not currently have an associated maintainer.
        ▪ Using image quay.io/metallb/speaker:v0.9.6
        ▪ Using image quay.io/metallb/controller:v0.9.6
    🌟  The 'metallb' addon is enabled
    💡  metrics-server is an addon maintained by Kubernetes. For any concerns contact minikube on GitHub.
    You can view the list of minikube maintainers at: https://github.com/kubernetes/minikube/blob/master/OWNERS
        ▪ Using image registry.k8s.io/metrics-server/metrics-server:v0.6.4
    🌟  The 'metrics-server' addon is enabled
    💡  dashboard is an addon maintained by Kubernetes. For any concerns contact minikube on GitHub.
    You can view the list of minikube maintainers at: https://github.com/kubernetes/minikube/blob/master/OWNERS
        ▪ Using image docker.io/kubernetesui/dashboard:v2.7.0
        ▪ Using image docker.io/kubernetesui/metrics-scraper:v1.0.8
    💡  Some dashboard features require the metrics-server addon. To enable all features please run:

      minikube addons enable metrics-server	


    🌟  The 'dashboard' addon is enabled
    Installing 'olm-v0.27.0' ...
    customresourcedefinition.apiextensions.k8s.io/catalogsources.operators.coreos.com created
    customresourcedefinition.apiextensions.k8s.io/clusterserviceversions.operators.coreos.com created
    customresourcedefinition.apiextensions.k8s.io/installplans.operators.coreos.com created
    customresourcedefinition.apiextensions.k8s.io/olmconfigs.operators.coreos.com created
    customresourcedefinition.apiextensions.k8s.io/operatorconditions.operators.coreos.com created
    customresourcedefinition.apiextensions.k8s.io/operatorgroups.operators.coreos.com created
    customresourcedefinition.apiextensions.k8s.io/operators.operators.coreos.com created
    customresourcedefinition.apiextensions.k8s.io/subscriptions.operators.coreos.com created
    customresourcedefinition.apiextensions.k8s.io/catalogsources.operators.coreos.com condition met
    customresourcedefinition.apiextensions.k8s.io/clusterserviceversions.operators.coreos.com condition met
    customresourcedefinition.apiextensions.k8s.io/installplans.operators.coreos.com condition met
    customresourcedefinition.apiextensions.k8s.io/olmconfigs.operators.coreos.com condition met
    customresourcedefinition.apiextensions.k8s.io/operatorconditions.operators.coreos.com condition met
    customresourcedefinition.apiextensions.k8s.io/operatorgroups.operators.coreos.com condition met
    customresourcedefinition.apiextensions.k8s.io/operators.operators.coreos.com condition met
    customresourcedefinition.apiextensions.k8s.io/subscriptions.operators.coreos.com condition met
    namespace/olm created
    namespace/operators created
    serviceaccount/olm-operator-serviceaccount created
    clusterrole.rbac.authorization.k8s.io/system:controller:operator-lifecycle-manager created
    clusterrolebinding.rbac.authorization.k8s.io/olm-operator-binding-olm created
    olmconfig.operators.coreos.com/cluster created
    deployment.apps/olm-operator created
    deployment.apps/catalog-operator created
    clusterrole.rbac.authorization.k8s.io/aggregate-olm-edit created
    clusterrole.rbac.authorization.k8s.io/aggregate-olm-view created
    operatorgroup.operators.coreos.com/global-operators created
    operatorgroup.operators.coreos.com/olm-operators created
    clusterserviceversion.operators.coreos.com/packageserver created
    catalogsource.operators.coreos.com/operatorhubio-catalog created
    Waiting for deployment "olm-operator" rollout to finish: 0 of 1 updated replicas are available...
    deployment "olm-operator" successfully rolled out
    Waiting for deployment "catalog-operator" rollout to finish: 0 of 1 updated replicas are available...
    deployment "catalog-operator" successfully rolled out
    Package server phase: Installing
    Package server phase: Succeeded
    deployment "packageserver" successfully rolled out
    Subscribing to ndmspc-operator form operatorhub.io ...
    subscription.operators.coreos.com/my-ndmspc-operator created
    service/keycloak created
    deployment.apps/keycloak created
    ingress.networking.k8s.io/keycloak created

    Keycloak:                 https://keycloak.192.168.49.2.nip.io
    Keycloak Admin Console:   https://keycloak.192.168.49.2.nip.io/admin
    Keycloak Account Console: https://keycloak.192.168.49.2.nip.io/realms/myrealm/account


    k8s dashboard:
      run : $ minikube dashboard


    To apply default config do :
      curl -s https://gitlab.com/ndmspc/ndmspc-operator/-/raw/main/config/samples/apps_v1alpha1_ndmspcconfig.yaml | sed 's/127.0.0.1.nip.io/192.168.49.2.nip.io/' | kubectl create -f -
    ```

## Make sure that ndmspc-operator is installed

Wait for operator to be installed and create ndmspc cluster
```
$ kubectl get csv
No resources found in default namespace.
```
```
$ kubectl get csv
NAME                      DISPLAY           VERSION   REPLACES                  PHASE
ndmspc-operator.v0.11.6   NdmSpc operator   0.11.6    ndmspc-operator.v0.11.5   Pending
```
Check if the operator is installed (PHASE collon has to be Succeeded)

```
$ kubectl get csv
NAME                      DISPLAY           VERSION   REPLACES                  PHASE
ndmspc-operator.v0.11.6   NdmSpc operator   0.11.6    ndmspc-operator.v0.11.5   Succeeded
```
## Deploy ndmspc cluster

When the operator is ready (`Succeeded`) one can apply the config by running following command

```
$ curl -s https://gitlab.com/ndmspc/ndmspc-operator/-/raw/main/config/samples/apps_v1alpha1_ndmspcconfig.yaml | sed 's/127.0.0.1.nip.io/192.168.49.2.nip.io/' | kubectl create -f -
ndmspcconfig.apps.ndmspc.io/ndmspc-sample created
```

Check if the cluster is ready

???+ info "Cluster status"

    ```
    $ kubectl get all,ingress,csv
    NAME                                            READY   STATUS    RESTARTS   AGE
    pod/keycloak-7575b57455-zf4gz                   1/1     Running   0          14m
    pod/ndmspc-sample-executor-7bcbb64b5-zndks      1/1     Running   0          2m56s
    pod/ndmspc-sample-ndmspc-web-74fd58cf6b-gdvtv   1/1     Running   0          2m39s
    pod/ndmspc-sample-ndmvr-65d9bdbcb9-xtkgm        1/1     Running   0          2m59s
    pod/ndmspc-sample-slsd-5cc96ffb9c-nbgtq         1/1     Running   0          2m48s
    pod/ndmspc-sample-slsr-5957c4f4cb-cj8vv         1/1     Running   0          2m46s
    pod/ndmspc-sample-slsw-7d8c7f8cf9-n9s6m         1/1     Running   0          2m44s
    pod/ndmspc-sample-slsw-7d8c7f8cf9-w9nft         1/1     Running   0          2m44s
    pod/ndmspc-sample-zmq2ws-67459675fd-99k4p       1/1     Running   0          3m5s

    NAME                                          TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
    service/keycloak                              LoadBalancer   10.101.92.118    <pending>     8080:31280/TCP   14m
    service/kubernetes                            ClusterIP      10.96.0.1        <none>        443/TCP          15m
    service/ndmspc-sample-executor-service        ClusterIP      10.103.209.74    <none>        41001/TCP        2m57s
    service/ndmspc-sample-ndmspc-web-service      ClusterIP      10.96.21.126     <none>        10000/TCP        2m42s
    service/ndmspc-sample-ndmvr-ws-service        ClusterIP      10.103.233.179   <none>        8443/TCP         3m1s
    service/ndmspc-sample-sls-discovery-service   ClusterIP      None             <none>        40000/TCP        2m49s
    service/ndmspc-sample-sls-mon-service         ClusterIP      10.111.194.132   <none>        5001/TCP         2m54s
    service/ndmspc-sample-sls-submitter-service   ClusterIP      10.100.62.195    <none>        41000/TCP        2m51s
    service/ndmspc-sample-zmq2ws-mgr-service      ClusterIP      10.111.102.195   <none>        10000/TCP        3m7s
    service/ndmspc-sample-zmq2ws-ws-service       ClusterIP      10.99.218.43     <none>        8442/TCP         3m8s

    NAME                                       READY   UP-TO-DATE   AVAILABLE   AGE
    deployment.apps/keycloak                   1/1     1            1           14m
    deployment.apps/ndmspc-sample-executor     1/1     1            1           2m56s
    deployment.apps/ndmspc-sample-ndmspc-web   1/1     1            1           2m39s
    deployment.apps/ndmspc-sample-ndmvr        1/1     1            1           2m59s
    deployment.apps/ndmspc-sample-slsd         1/1     1            1           2m48s
    deployment.apps/ndmspc-sample-slsr         1/1     1            1           2m46s
    deployment.apps/ndmspc-sample-slsw         2/2     2            2           2m44s
    deployment.apps/ndmspc-sample-zmq2ws       1/1     1            1           3m5s

    NAME                                                  DESIRED   CURRENT   READY   AGE
    replicaset.apps/keycloak-7575b57455                   1         1         1       14m
    replicaset.apps/ndmspc-sample-executor-7bcbb64b5      1         1         1       2m56s
    replicaset.apps/ndmspc-sample-ndmspc-web-74fd58cf6b   1         1         1       2m39s
    replicaset.apps/ndmspc-sample-ndmvr-65d9bdbcb9        1         1         1       2m59s
    replicaset.apps/ndmspc-sample-slsd-5cc96ffb9c         1         1         1       2m48s
    replicaset.apps/ndmspc-sample-slsr-5957c4f4cb         1         1         1       2m46s
    replicaset.apps/ndmspc-sample-slsw-7d8c7f8cf9         2         2         2       2m44s
    replicaset.apps/ndmspc-sample-zmq2ws-67459675fd       1         1         1       3m5s

    NAME                                                     CLASS   HOSTS                          ADDRESS        PORTS     AGE
    ingress.networking.k8s.io/keycloak                       nginx   keycloak.192.168.49.2.nip.io   192.168.49.2   80, 443   14m
    ingress.networking.k8s.io/ndmspc-sample-ingress-ndmspc   nginx   ndmspc.192.168.49.2.nip.io     192.168.49.2   80        2m37s

    NAME                                                                 DISPLAY           VERSION   REPLACES                  PHASE
    clusterserviceversion.operators.coreos.com/ndmspc-operator.v0.11.6   NdmSpc operator   0.11.6    ndmspc-operator.v0.11.5   Succeeded
    ```

## Access ndmspc webpage
Now one can access ndmspc service at [http://ndmspc.192.168.49.2.nip.io](http://ndmspc.192.168.49.2.nip.io)

## Stop and start cluster
One can use `minikube` to stop (`minikube stop`) and start (`minikube start`) cluster again.