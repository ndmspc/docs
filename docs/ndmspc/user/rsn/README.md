# NDMSPC Resonance example

### NDMSPC prerequisites:
 - [aliBuild](https://alice-doc.github.io/alice-analysis-tutorial/building/)/[cvmfs](https://gitlab.com/ndmspc/user/-/blob/main/services/cvmfs/README.md?ref_type=heads) for installing ndmspc framework
 - for running on MacOS set variable: `export ROOT_INCLUDE_PATH="/opt/homebrew/Cellar/nlohmann-json/3.11.3/include"`

### Installation
TO DO [#25](https://gitlab.com/ndmspc/docs/-/issues/25)

### Set up analysis environment

This example demonstrates ndmspc functionality for analysing resonance data. Start by creating/navigating to your folder of choice.

```
mkdir ndmspc-tutorial
cd ndmspc-tutorial 
```

For purposes of following this instruction you can download example file `data.root` and save it to the relevant folder.
```
curl -L -o data.root https://eos.ndmspc.io/eos/ndmspc/scratch/tutorial/rsn/data.root
```
Enter the NDMSPC enviroment by
```
alienv enter ndmspc/latest
```

If using cvmfs, you might have to run `source /cvmfs/alice.cern.ch/etc/login.sh` before executing the above command.

### Running Analysis

For generating basic initial macro `NdmspcPointMacro.C` run
```
root $NDMSPC_MACRO_DIR/NdmspcInit.C'("data.root","phianalysis-t-hn-sparse_std/unlike","axis1-pt")'
```
You can get arguments filled in the brackets by opening `data.root` file and 

 1. Listing all directory files which root file consist of.
    ```
    [ndmspc/latest] ~/ndmspc-tutorial %> root data.root
    ------------------------------------------------------------------
    | Welcome to ROOT 6.30/01                        https://root.cern |
    | (c) 1995-2023, The ROOT Team; conception: R. Brun, F. Rademakers |
    | Built for macosxarm64 on Apr 09 2024, 06:51:43                   |
    | From tags/v6-30-01-alice4@v6-25-02-6402-g7bd79f26a4              |
    | With Apple clang version 15.0.0 (clang-1500.3.9.4)               |
    | Try '.help'/'.?', '.demo', '.license', '.credits', '.quit'/'.q'  |
    ------------------------------------------------------------------

    root [0] 
    Attaching file data.root as _file0...
    (TFile *) 0x12e129ca0
    root [1] .ls
    TFile**		data.root	
    TFile*		data.root	
    KEY: TDirectoryFile	collision-converter;1	collision-converter
    KEY: TDirectoryFile	bc-selection-task;1	bc-selection-task
    KEY: TDirectoryFile	event-selection-task;1	event-selection-task
    KEY: TDirectoryFile	{==phianalysis-t-hn-sparse_std==};1	phianalysis-t-hn-sparse_std
    ```
2. Displaying objects it contains 
    ```
    root [2] gFile->Get("phianalysis-t-hn-sparse_std")->ls()
    TDirectoryFile*		phianalysis-t-hn-sparse_std	phianalysis-t-hn-sparse_std
    KEY: TH1F	hVz;1	
    KEY: THnSparseT<TArrayF>	{==unlike==};1	Unlike
    KEY: THnSparseT<TArrayF>	likepp;1	Like PP
    KEY: THnSparseT<TArrayF>	likemm;1	Like MM
    ```
3. Extracting axes names from chosen `THnSparseT` object
    ``` 
    root [3] ((THnSparse*)gFile->Get("phianalysis-t-hn-sparse_std/unlike"))->GetListOfAxes()->ls()
    OBJ: TObjArray	TObjArray	An array of objects : 0
    OBJ: TAxis	axis0-im	Inv. mass (GeV/c^{2}) : 0 at: 0x600002b4c380
    OBJ: TAxis	{==axis1-pt==}	p_{T} (GeV/c) : 0 at: 0x600002b4c460
    OBJ: TAxis	axis2-mu	N : 0 at: 0x600002b4c540
    OBJ: TAxis	axis3-ns1	nSigma of positive particle (0.493677) : 0 at: 0x600002b4c620
    OBJ: TAxis	axis4-ns2	nSigma of negative particle (0.493677) : 0 at: 0x600002b4c700
    OBJ: TAxis	axis5-y	y : 0 at: 0x600002b4c7e0

    ```
Or simply check it by opening `TBrowser()`.

<figure markdown="span">
  ![](pic1.png){width="80%"}
  <figcaption>TBrowser</figcaption>
</figure>


If you are using above mentioned example file it is not necessery to fill additional arguments and simply use default values.
```
root $NDMSPC_MACRO_DIR/NdmspcInit.C
```
First thing you can notice after opening this generated macro is configuration part, where in part `["data"]` are predefined values we filled in arguments above.
```
void NdmspcDefaultConfig(json & cfg)
{
  cfg = R"(

  ...

    "data": {
      "file": {=="data.root"==},
      "objects": [
        {=="phianalysis-t-hn-sparse_std/unlike"==}
      ]
    },

  ...

)"_json;
}
```
followed by the process itself.

Now you can run the macro with default configuration by command
```
 root $NDMSPC_MACRO_DIR/NdmspcPointRun.C 
```
The output is shown in canvas and written in `content.root` file. Here you can see sumarized invariant mass projection with no cuts included.

<figure markdown="span">
  ![](pic2.png){width="80%"}
  <figcaption>Default output.</figcaption>
</figure>

We can see it is just basic 1D projection made of THnSparse object, but feel free to implement the process part by yourself. 

For example we can add parameter of the minimal required entries for analysis. We can do it by adding parameter `minEntries` to configuration part `["ndmspc"]`and implementing requirement in process part.
``` json
 "ndmspc": {
    ...

    "minEntries": -1,

    ...
 }
```
```
TList *NdmspcProcess(TList *inputList, json cfg, THnSparse *finalResults, Int_t *point)
{
  ...

 THnSparse *hs = (THnSparse *)inputList->At(0);
 TH1D *h = hs->Projection(projId, "O");
 h->SetNameTitle("h", TString::Format("h %s", titlePostfix.Data()).Data());

 {== if (h->GetEntries() < cfg["ndmspc"]["minEntries"].get<int>()) ==}
  {==  return nullptr; ==}

  ...

}
```

As another example we can add functionality of counting value of integral. To see written output in configuration set `["ndmspc"]["verbose"]` nonezero value .
```
TList *NdmspcProcess(TList *inputList, json cfg, THnSparse *finalResults, Int_t *point)
{
    ...

  Double_t integral;
  integral = h->Integral();

  if (cfg["ndmspc"]["verbose"].get<int>() >= 1)
    Printf("Integral = %f ", integral);

    ...

}

```
The output looks like that 
```
root [0] 
Processing $NDMSPC_MACRO_DIR/NdmspcPointRun.C...
Opening file 'data.root' ...
Opening obj='phianalysis-t-hn-sparse_std/unlike' ...
{==Integral = 11713338.000000==} 
Info in <TCanvas::MakeDefCanvas>:  created default TCanvas with name c1
Objects stored in 'content.root'
(int) 0
```

Now we would like to fill `FinalResults` sparse with values of our integral. We can use part of configuration named `["ndmspc"]["result"]["names"]` and implement sparse filling.
``` json
 "result": {
      "names": ["integral"]
    },
```
```
if (finalResults) 
    outputList->Add(finalResults);

  Long64_t frbin;

  for (auto &name : cfg["ndmspc"]["result"]["names"])
  {
    std::string n = name.get<std::string>();
    if (!n.compare("integral"))
    {
      point[0] = 1;
      frbin = finalResults->GetBin(point);
      finalResults->SetBinContent(point, integral);
    }
  }
```

In output file `content.root` you can see new object now called `results` where the first axis is filled with parameters in our case it is only integral.

<figure markdown="span">
  ![](pic6.png){width="80%"}
  <figcaption>Final results.</figcaption>
</figure>

Now you can look into the configuration and change some parameters to see how the output differs.

In part `["ndmspc"]["cuts"]` you can enable the functionality of cuts by adding configurables as can be seen below. You can choose axis and range you want to cut and also rebin if you would like. Cuts can be disabled by changing value of `["enabled"]` to false.
``` json
    "cuts": [
      {
        "axis": "axis1-pt",
        "bin": {
          "max": 3,
          "min": 3
        },
        "enabled": true,
        "rebin": 1
      }
    ],
```

In new output we can see invariant mass projection for $p_{T}$ in range [2.00 , 3.00] GeV

<figure markdown="span">
  ![](pic3.png){width="80%"}
  <figcaption>Cut enabled output.</figcaption>
</figure>

By changing values of `["bin"]["max"]` and `["bin"]["min"]` we can choose range of $p_{T}$.

``` json
    "bin": {
            "max": 4,
            "min": 4
            },
```

<figure markdown="span">
  ![](pic4.png){width="80%"}
  <figcaption>Different range output.</figcaption>
</figure>

<figure markdown="span">
  ![](pic5.png){width="80%"}
  <figcaption>Different range output.</figcaption>
</figure>

To run this process through all bins you firstly need to generate separate config file by `NdmspcAnalysisCli.C` macro which will be also used later for synchronizing with web interface ([See more here.](../web/README.md))
```
root $NDMSPC_MACRO_DIR/NdmspcAnalysisCli.C'("myAnalysis")'
```
You can see two new files created in your tutorial folder: `myAnalysis.json` which is file with your configuration; and `myAnalysis.ndmspc` which is used for configuring the web interface.

When you open `myAnalysis.json` you find out part `["ndmspc"]["output"]` has been rewritten.
``` json
"output": {
      "dir": "/eos/ndmspc/scratch/temp/username/myAnalysis/axis1-pt/bins",
      "file": "content.root",
      "host": "root://eos.ndmspc.io/",
      "opt": "?remote=1"
    },
```
For running process for all bins set 
``` json
  "process": {
      "type": "all"
  },
```
 By using `myAnalysis.json` configuration your output files will be saved on EOS in separate folders. To use this configuration run:
```
root $NDMSPC_MACRO_DIR/NdmspcPointRun.C'("myAnalysis.json")' -b -q
```
In the output you can see where are stored your `content.root` files
```
root [0] 
Processing /Users/veronikabarbasova/alice/sw/osx_arm64/ndmspc/46-functionality-for-tutorial-part-3-local1/macros/NdmspcPointRun.C("myAnalysis.json")...
Opening file 'root://eos.ndmspc.io//eos/ndmspc/scratch/temp/username/myAnalysis/inputs/data.root' ...
Opening obj='phianalysis-t-hn-sparse_std/unlike' ...
ibin=1
Processing 'axis1-pt[0.00,1.00]' ...
Integral = 713356.000000 
Info in <TCanvas::MakeDefCanvas>:  created default TCanvas with name c1
Objects stored in '{==root://eos.ndmspc.io//eos/ndmspc/scratch/temp/username/myAnalysis/axis1-pt/bins==}/1/content.root'
ibin=2
Processing 'axis1-pt[1.00,2.00]' ...
Integral = 8516925.000000 
Objects stored in '{==root://eos.ndmspc.io//eos/ndmspc/scratch/temp/username/myAnalysis/axis1-pt/bins==}/2/content.root'
...
```

Another macro will merge all `content.root` files into one. As first argument you need to provide path to the relevant `bins` folder which you can find in previous output. By second argument you can specify location where you want to save merged file.
```
root $NDMSPC_MACRO_DIR/NdmspcMerge.C'("{==root://eos.ndmspc.io//eos/ndmspc/scratch/temp/username/myAnalysis/axis1-pt/bins==}" , "/myFolder/merged.root")' -q
```

To see a distribution of integral over the $p_{T}$ bins let's:

0. open `merged.root` file in root,  
1. take out `results` sparse,
2. set range for $p_{T}$ axis,
3. set range for params axis,
4. create a projection,
5. draw it.
```
root [0] TFile *merged = TFile::Open("/myFolder/merged.root")
root [1] THnSparseD* sResults = (THnSparseD*)merged->Get("results")
root [2] sResults->GetAxis(1)->SetRangeUser(0.0, 20.0)
root [3] sResults->GetAxis(0)->SetRange(1, 1)
root [4] TH1* h = sResults->Projection(1)
root [5] h->Draw("text" "E1")
```

<figure markdown="span">
  ![](pic7.png){width="80%"}
  <figcaption>Distribution of integral.</figcaption>
</figure>

<!-- ### Web Interface

Now we are ready to move on to web interface environment. Firstly we need to copy our example file and macro to eos folder with `NdmspcAnalysisCli.C` macro where the first argument is name of your analysis. Also you use it to synchronize your changes in `NdmspcPointMacro.C` with web interface. 
```
root $NDMSPC_MACRO_DIR/NdmspcAnalysisCli.C'("myAnalysis")'
```
You can see two new files created in your tutorial folder: `myAnalysis.json` which is file with your configuration; and `myAnalysis.ndmspc` which is used for configuring the web interface.

In command line you can run `NdmspcPointRun.C` using configuration file like that
```
root $NDMSPC_MACRO_DIR/NdmspcPointRun.C'("myAnalysis.json")' -b -q
``` -->


