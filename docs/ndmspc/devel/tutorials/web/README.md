# NDMSPC Web developer guide

This developer guide is intended to provide all necessary information the web developer could and will need. The information include a basic project initialization (React application templated with vite.js packager). Then will be explained how to implement a simple visualization component that depends on few components for visualization with necessary relations between them.

## Requirements definition of the example project

A basic visualization component consists of 4 components such as:

- **HistogramComponent:** visualization of the histogram using NDMVR component
- **BinInfoComponent:** visualization of the projection for selected bin
- **BinColorChangeComponent:** component to simulate use case from user input by button (change of the color)
- **ProjectionComponent:** component to serve projection of selected bin for the aside panel and VR panel

The second main part of the implemented visualization are user's interactions.
In this component we will implement 3 basic interactions:

- **hover** - it shows a current bin info on the aside panel (with selected bin count)
- **click** - it shows a projection for the current bin on the panel
- **dbClick** - it selects or deselects the bin 

Now we have defined basic project specifications.

## Project initialization

Project will present react application with all components and functions.

First, we need initialize the React app. 

### Create simple react application using template packager Vite

we can create app using npm:

```bash
# npm 6.x
npm create vite@latest my-react-app --template react

# npm 7+, extra double-dash is needed:
npm create vite@latest my-react-app -- --template react

# yarn
yarn create vite my-react-app --template react

# pnpm
pnpm create vite my-react-app --template react
```

After creating an application we have a simple project 

The next step we need to install all important dependencies:

### Install NDMSPC/REACT-NDMSPC-CORE

Contains all necessary functions and component important for implementation.
Includes all functionalities from JSROOT library, which are imported in case of dependency reduction.

```bash
npm i @ndmspc/react-ndmspc-core --save
```

### Install NDMSPC/NDMVR

Dependency contains component **NdmVrScene** for visualization data in VR environment.

```bash
npm i @ndmspc/ndmvr --save
```

After that all, we have prepared project for implementation.
Starting development server with:

```bash
npm run dev
```

After starting project you have a running application on ` http://localhost:5173/`

![](images/runningApp.png "running application after project initialization")

## Add HistogramComponent

It defines a component with displayed histogram in VR. 
For this purpose we create a new component in directory components, called for example `HistogramComponent.jsx` where we implement that functionality.

```
|-assets
|-components
    |--HistogramComponent.jsx
|-App.jsx
|-...other files
```

For demonstration of the behaviour of interaction in this visualization we create demo object that will represent the example histogram, which will be displayed in VR.
For this we can implement s simple function for create TH2 object a we can configure axes and contents as that:

```javascript title="utils.js"
// get random number in range 0 ... 10 
function getRandomInt() {
    return Math.floor(Math.random() * 10);
}

// create simple histogram TH2 with scales 6x6 using JSROOT
const createTh2Histogram = () => {
    const histo = createHistogram('TH2I', 6, 6)
    for (let iy=1; iy<=6; iy++)
        for (let ix=1; ix<=6; ix++) {
            let bin = histo.getBin(ix, iy)
            histo.setBinContent(bin, getRandomInt());
        }
    histo.fXaxis.fTitle = 'x Axis'
    histo.fYaxis.fTitle = 'y Axis'
    histo.fName = "example"
    histo.fTitle = "example"
    histo.fMaximum = 10

    return histo
}
```

We create this function in separate file called `utils.js` in directory `utils`. We will create this object in main application script `App.js`.

After that we can implement the component for displaying our histogram with using NDMVR component for visualizations, `NdmVrScene`,

```javascript title="HistogramComponent.jsx"
import {NdmVrScene} from "@ndmspc/ndmvr";
import {createTh2Histogram} from "../utils/utils.js";


const HistogramComponent = ({ histogram }) => {

    // coniguration data 
    const histoConfigData = {
        histogram: histogram,  // histogram object, created above
        id: 'example',         // identifier of histogram, 
        projectionsNames: [],  // we can define proj names, which are displayed
        projPanelIds: []       // we can define proj panel id's in VR enviro, which are displayed
    }
    
    // React component jsx return value  
    return <NdmVrScene
        data={histoConfigData}
    />
}

export default HistogramComponent
```

In this example we have created a simple visualization component. 
We use this component in main application script `App.jsx` and run application 

![](images/componentApp.png "running application with displayed HistogramComponent")

!!! note
    [Project state](https://gitlab.com/ndmspc/tutorials/ndmspc-developer/-/tree/e5f7ac83d0916e4199afd0be83eba3373f51d26b)

## Add BinInfoComponent and implement show bin info on hover

The next step of the implementation is to ensure displaying the bin info on aside panel.
After user intersects the bin of the histogram we need to get info a paste this info on other elements in the application.
Here we already need to create another component called `BinInfoComponent.jsx` that we create in directory `components`
here is implementation of this component:

```javascript title="BinInfoComponent.jsx"
import {useState} from "react";
import {appGlobalScope} from "../AppGlobalContext.js";

const BinInfo = () => {
    // bin Info to display
    const [binInfo, setBinInfo] = useState(null)

    // React component jsx return value  
    return <div className="panel">
        <h2>Selected bin for histogram {binInfo?.data?.id}</h2>
        <p className="line x-axis"><b>x axis:</b>{binInfo?.data?.binx}</p>
        <p className="line y-axis"><b>y axis:</b>{binInfo?.data?.biny}</p>
        <p className="line"><b>intersection:</b>{binInfo?.data?.intersectionAxis}</p>
        <p className="line"><b>content:</b>{binInfo?.data?.con}</p>
    </div>
}

export default BinInfo
```

And we have to update the main `App.jsx` with use this new component like that:

```javascript title="App.jsx" hl_lines="9 12"
const App = () => {

    // create demo histogram object
    const histogram = createTh2Histogram()

    return (
      <div className="main">
          <div className="main-section">
              <HistogramComponent />
          </div>
          <div className="aside-section">
              <BinInfo />
          </div>
      </div>
    )
}

export default App
```

![](images/histogramWithBinInfo.png "running application with displayed HistogramComponent and BinInfo without communication")

After this part we have both components, and we need to connect these components for case of sending and displaying data.
We need to ensure state of the bins which are selected or not and distribution of that bins among components in the application.
For this purpose have to create the global application context for storing this state and communication objects called distributors.

The next step, we need to create this app context, we create the script `AppGlobalContext.js` where we implemented a whole logic of app context.
Let's start with import all needed classes for build our `appGlobalScope` object.
After importing classes we need to determine our data structure as state. We need selected bins, our data structure can be simple array with stored string values of the bin id's.
Our `appGlobalScope` could be implemented like that:

```javascript title="AppGlobalContext.js" hl_lines="5"
import {AppGlobalScope} from "@ndmspc/react-ndmspc-core";

// create instance of this global scope and initialize data structure for state of app
const appGlobalScope = new AppGlobalScope({
    bins: new Map()
})
```

After `appGlobalScope` has been initialized, we need to ensure communication and data distribution among components.
For that we import Distributor class and create the instance of that for purpose of distribution to components in whole application. 
We will use this distributor for sending the `binData`. We have to set the name for this distributor (in this case the name can represent the event)
After creation of this instance we need add this component into `AppGlobalScope` like that:

```javascript title="AppGlobalContext.js"
import {AppGlobalScope, Distributor} from "@ndmspc/react-ndmspc-core";

appGlobalScope.addDistributor(new Distributor("hover"));
```

In this example, we have created a distributor with name **hover** for distribution of data on hover.
Then we need to define a process of data computation and the interaction logic.
We implement a logic at both ends of the communication channel, created by **distributor**.

- On the side of `AppGlobalScope` we have to implement `input function` for handling and computing received data from out of the current scope (for example from any component in the application).
- On the side of the component somewhere in the application, we have to implement `output function` for handling and computing data from `appGlobalScope`.

Here we distinguish 2 kinds of subscriptions on appropriate distributor: 

- **Internal subscription** - when we set `input function` as handler on distributor observable object, we don't have direct access to this subscription reference, this reference is managed in scope of the `AppGlobalScope` object.
- **External subscription** - when we set `output function` as handler on distributor observable object in any component in the app, we need direct access to this reference for administration. We can manage this subscription.

Now, we need to implement an `input function` in `AppGlobalScope` for hover event, and so we implement this functionality in file `AppGlobalContext.js`.

```javascript title="AppGlobalContext.js" hl_lines="6 13"
// input function implementation
const handleInputHoverFunc = (binData) => {
    if (binData) {
        appGlobalScope
            .getDistributorById("hover")
            ?.sendOutputData(binData);
    }
};

// register function on appropriate distributor for handling 
appGlobalScope.addHandlerFunc(
    "hover",
    handleInputHoverFunc,
    null
);

// set subscriptions for launch communications from this main object
appGlobalScope.addAllSubscriptions();
```

Above we have the implementation of `input function` with registration in `AppGlobalScopes` as a handler for distributor celled **hover**.
Then, we launch this communication.

This handler receives a parameter (binData) and send this data as `sendOutputData` (data for subscription in the output visualization components, received in **external subscriptions**)
Other use case is the usage of `sendInputData` where we publish data which are received and processed in `AppGlobalScope` (received in **internal subscriptions**).
As last option is `sendCustomData` where we can publish data which are received at both ends (in **internals and external subscriptions**)

After this implementation we need to implement handler for the hover interaction in the histogram.
This we can complete in this script as well.

```javascript title="AppGlobalContext.js"
// handler for processing data from component 
const handleHover = (data) => {
    appGlobalScope.getDistributorById("hover")?.sendInputData({
        id: `${data.id}-${data.binx}-${data.biny}`,
        data: data
    })
}
```

Finally, we export all important object and functions such as `AppGlobalScope` and callback function `handleHover`.

In the other side we have to modify our visualization components `HistogramComponent` and `BinInfo` to enable interactions.

### Append hover functionality in HistogramComponent

Here we need only import implemented callback function and insert this function into component `NdmVrScene`.

```javascript title="HistogramComponent.jsx"
// ... other imports
import {handleHover} from "../AppGlobalContext.js";

const HistogramComponent = ({ histogram }) => {

    // ... other functionality

    // React component jsx return value  
    return <NdmVrScene
        // ... other props
        onHover={handleHover}
    />
}
```

This function receives and publishes **binData** on distributor called **hover**

### Append hover functionality in binInfo

In this component we need to subscribe data about hovered bin.

We implement `useEffect` where we create the external subscription from distributor called **hover**. 
Then we have to implement the handler function what in this case changes the state of this component.
Finally, the component refresh entities based on the state.

```javascript title="BinInfo.jsx"
// ... other imports 
import {appGlobalScope} from "../AppGlobalContext.js";

const BinInfo = () => {
    // bin Info to display
    const [binInfo, setBinInfo] = useState(null)

    // handler function for receiving and setting a binInfo
    const handleDisplayingProjection = (binData) => {
        setBinInfo(binData);
    }

    // create a subscription after rendering component
    useEffect(() => {
        const subscription = appGlobalScope.createExternalSubscription(
            'hover',
            handleDisplayingProjection,
            null
        )
        return () => subscription.unsubscribe()
    }, [])

    // ... other functionality
    
    // ... return clause
}

export default BinInfo
```

Final result is interaction on hover in the histogram, and then it is displayed bin information on the left aside panel.

![](images/hoverInteraction.png "hover interaction and displayed data on the left side panel")

!!! note
    [Project state](https://gitlab.com/ndmspc/tutorials/ndmspc-developer/-/tree/442aa0cb6baf4b616db16723d0092299bff02473)

## Add ColorChangeComponent and implement change color

In directory `components`, we have to create component `ColorChangeComponent` where we create state of the component, and we implement JSX return clause.

```javascript title="ColorChangeComponent.jsx"
import "../styles.css"
import {useState} from "react";

const ColorChangeComponent = () => {
    // color state of the component
    const [color, setColor] = useState('yellow');

    const handleSetColor = () => {
        // confirm selected color logic
    }

    // inpu change handler
    const handleChange = (event) => {
        setColor(event.target.value)
    }

    // React component jsx return value  
    return 
    <div className="panel">
        <h2>Color configurator for Component 1 histogram</h2>
        <div className="line">
            <input className="color-input" type="color" value={color} onChange={handleChange}/>
            <button className="confirm-button" onClick={handleSetColor}>Set color</button>
        </div>
    </div>
}

export default ColorChangeComponent
```

![](images/colorChangeComponent.png "add ColorChangeComponent without any connections")

### Add interaction on change color of the bins

Firstly, we need to edit `AppGlobalContextContext.js`, we have to add state for `colorConfig` in object `AppGlobalScope` and then we need create a new distributor with name **colorChange**.
After that, we can continue with `input function` implementation for this distributor.

```javascript title="AppGlobalContext.js"
// ... other imports

const appGlobalScope = new AppGlobalScope({
    color: null
})

appGlobalScope.addDistributor(new Distributor("colorChange"));
// ... other distributor creations

// input function to modify incoming data and set global scope states
const handleInputFunc = (config) => {
    if (config) {
        console.log(config);
        appGlobalScope.state.conf = {
            color: config
        };
        appGlobalScope
            .getDistributorById("config")
            ?.sendOutputData(config);
    }
};
// ... other handleInputFunc implementations

appGlobalScope.addHandlerFunc(
    "colorChange",
    handleInputHoverFunc,
    null
);
// ... other handleInputFunc registrations
```

We do not need to implement any handler for interaction.

### Add interaction on change color of the bins in external component

The last step is the subscription of the received data in the external component and change state of the component.
We add the handler for changing and publishing a color to internal component.
We add these functions in component `ColorChangeComponent.jsx`:

```javascript title="ColorChangeComponent.jsx"
import {appGlobalScope} from "../AppGlobalContext.js";

// ... other implementation

// publish current state to appGlobalScope
const handleSetColor = (config) => {
    appGlobalScope.getDistributorById('colorChange').sendInputData(color)
}

// subscribe data and main functionality
useEffect(() => {
    const subscription = appGlobalScope.createExternalSubscription(
        'colorChange',
        (currentColor) => { setColor(currentColor) },
        null
    )
    return () => subscription.unsubscribe()
}, [])

// ... return clause
```

Now we have implemented the communication between `AppGlobalScope` and `ColorChangeComponent`. 
The next step is also to update the visualization component `HistogramComponent` and refresh color that depends on current value from global state.

There we need create the external function to receive this color as well as it is implemented in `ColorChangeComponent`.
For setting colors of the bins we implement another handler function and send this function to `NdmVrScene` component.

```javascript title="HistogramComponent.jsx"
// ... imports

const HistogramComponent = ({ histogram }) => {
    // state of the React component for color, initial state is global state 
    const [color, setColor] = useState(appGlobalScope.state.color)

    const handleBinColor = (data) => {
        return color
    }
    
    useEffect(() => {
        const subscription = appGlobalScope.createExternalSubscription(
            'colorChange', 
            (data) => { setColor(data) }, // handler to set current color state of the component 
            null
        )
        return () => subscription.unsubscribe()
    })
    
    // ... other functionality

    return <NdmVrScene
        // ... other props
        onView={handleBinColor}
    />
}
```

![](images/changeColor.png "colorChangeComponent without all connections and interactivity")

!!! note
    [Project state](https://gitlab.com/ndmspc/tutorials/ndmspc-developer/-/tree/1a37ad73c5b64859112f4ae404a6c76e8cbcdc99)

## Implement bin selection on DbClick

Firstly, we need to append another variable for state in `appGlobalScope`. For selected bins we create array with bin id's.
Each bin have a unique id in the scope of the histogram where it is situated, and then we can create bin id from histogram id, x coordinate and y coordinate.

!!! warning 
    Example of the bin **id** is for this example project, the template `${data.histogramId}-${data.x}-${data.y}`

In the file `AppGlobalContext` we create in state variable (Map for better management) and another distributor with name **binSelect** with appropriate `input function` and registrations, so on.
We need also implement a handler (`handleDbClick`) on dbClick in `NdmVrScene`, which is exported for sending to component as prop.

```javascript title="AppGlobalContext.js"
// ... other imports

const appGlobalScope = new AppGlobalScope({
    bins: new Map(),
    // ...other attributes
    
})

appGlobalScope.addDistributor(new Distributor("binSelect"));
// ... other distributor creations

// input function to modify incoming data and set global scope states
const handleInputBinSelectFunc = (binData) => {
    if (binData) {
        const selectedBin = appGlobalScope.state.bins.get(binData.id);
        if (selectedBin) {
            appGlobalScope.state.bins.delete(binData.id);
        } else {
            appGlobalScope.state.bins.set(binData.id, binData.data);
        }
        appGlobalScope
            .getDistributorById("binSelect")
            ?.sendOutputData(Array.from(appGlobalScope.state.bins.keys()));
    }
};
// ... other handleInputFunc implementations

appGlobalScope.addHandlerFunc(
    "binSelect",
    handleInputBinSelectFunc,
    null
);
// ... other handleInputFunc registrations

const handleDbClick = (data) => {
    appGlobalScope.getDistributorById("binSelect")?.sendInputData({
        id: `${data.id}-${data.binx}-${data.biny}`,
        data: data
    })
}
// ... other handlers exported for handling events
```

We need to update `HistogramComponent` as well. 

```javascript title="HistogramComponent.jsx" hl_lines="7 17"
// ... imports

const HistogramComponent = ({ histogram }) => {
    // state of the React component for color, initial state is global state 
    const [bins, setBins] = useState(Array.from(appGlobalScope.state.bins.keys()))

    const handleBinColor = (data) => {
        let finalColor = color
        bins?.forEach((x) => {
            if (x === `${data.histogramId}-${data.x}-${data.y}`) {
                finalColor = 'red'
            }
        })
        return finalColor
    }

    useEffect(() => {
        const subscription = appGlobalScope.createExternalSubscription(
            'binSelect',
            (data) => { setBins(data) },
            null
        )
        return () => subscription.unsubscribe()
    })
    
    // ... other functionality

    return <NdmVrScene
        // ... other props
        onDbClick={handleDbClick}    
    />
}
```

![](images/binSelection.png "HistogramComponent with the selection bins interactivity")

!!! note
    [Project state](https://gitlab.com/ndmspc/tutorials/ndmspc-developer/-/tree/729eb7cc7dba84f97a37fa840642a8716ea8499c)

## Implement show projection on click

This interaction shows a special view of displayed data using TH1 projection for certain axis.
We need create:

- handler function for handling click event when we click on the bin (send data about clicked bin from `NdmVrScene`)
- component for receiving this binData info with histogram th1 displayed in canvas 
- function to create projection by computing the histogram object and received bin info

We edit the main context of the application `AppGlobalContext.js` as in last examples.
We have to create new distributor with name **click**, its registration and an `input function` where we received and send`binData` to the output components.

!!! note
    The functionality is very similar as in case of **hover**, but we need to separate these functionalities !!! Based on that, we cannot use distributor for **hover**.

We add this distributor and `input function` with handler for click events in VR:

```javascript title="AppGlobalContext.js"
// ... other imports

appGlobalScope.addDistributor(new Distributor("click"));
// ... other distributor creations

// input function to modify incoming data and set global scope states
const handleInputClickFunc = (binData) => {
    if (binData) {
        appGlobalScope
            .getDistributorById("click")
            ?.sendOutputData(binData);
    }
};
// ... other handleInputFunc implementations

appGlobalScope.addHandlerFunc(
    "click",
    handleInputClickFunc,
    null
);
// ... other handleInputFunc registrations

// handler for processing data from component
const handleClick = (data) => {
    appGlobalScope.getDistributorById("click")?.sendInputData({
        id: `${data.id}-${data.binx}-${data.biny}`,
        data: data
    })
}
// ... other handlers exported for handling events

export {/*... other handlers*/ handleClick}
```

Then we need to implement the component where will be displayed the projection. 
In `components` we create component `ProjectionComponent.jsx`.

```javascript title="ProjectionComponent.jsx" hl_lines="5"
const ProjectionComponent = () => {
    
    // component logic
    
    return <div className="proj-panel" id="projection" />
}

export default ProjectionComponents
```

!!! warning
    When we define an element for displaying th1 histogram we need style this element with fixed width and height of the projection panel (not relative width and height) because there is an incompatibility with JSROOT!
    In this example it is styled in class `proj-panel`

In `App.jsx` we append this component into main layout:

```javascript title="App.jsx" hl_lines="13"
const App = () => {

    // component logic
    
    return (
      <div className="main">
          <div className="main-section">
              <HistogramComponent histogram={histogram} />
          </div>
          <div className="aside-section">
              <BinInfo />
              <ColorChangeComponent />
              <ProjectionComponent />
          </div>
      </div>
    )
}
```

At now, we have to implement logic of the `ProjectionComponent` there we have to:

- receive data about the bin
- create appropriate projection, defined by main histogram and bin info
- draw this projection on the panel 
- finally we need to get png texture of panel view with setting texture on entity in VR scene (`NdmVrScene`)

We add a functionality of receiving and computing data:

```javascript title="ProjectionComponent.jsx"
useEffect(() => {
    const subscription = appGlobalScope.createExternalSubscription(
        'click',
        handleCreateProjection,
        null
    )
    return () => subscription.unsubscribe()
}, [])
```

Then we add a functionality of creating and rendering a projection on panel with id **projection**. We have to implement handler `handleCreateProjection`
For this purpose we can use an existing function from library **react-ndmspc-core** `createTH1Projection`. 
As function parameters we define target projection axis, data (include binData and main histogram object), projection panel ids (as all target elements), and all axes for projection computation:

```javascript title="ProjectionComponent.jsx" hl_lines="5"
import {createTH1Projection} from "@ndmspc/react-ndmspc-core";

const handleCreateProjection = async (data) => {
    if (data) {
        await createTH1Projection('X', data.data, ['projection'], ['X', 'Y']) // create JSROOT histogram projection 
    }
}
```

!!! note
    This function generates the projection of data distribution fixed to axis X (as 1 dimensional data)

!!! warning
    To ensure a correct execution of this function we need use `async` function because this process is longer!!! Then we have to use `await` for waiting on the result asynchronous function.

Then we need update a `HistogramComponent` and append `handlerClick` function for handling event in `NdmVrScene`:

```javascript title="ProjectionComponent.jsx"
import {/* ... other objects */ handleClick} from "../AppGlobalContext.js";

// ... logic

return <NdmVrScene
    // ... other props
    onClick={handleClick}    
/>
```

Now, we can extend this function and add a condition for rendering the projection, that depends on firstly intersected side of the entity (if first intersection is detected on side, that corresponds with axis X, then it fires X intersection value).

We can modify handler function `handleCreateProjection` like that:

```javascript title="ProjectionComponent.jsx" hl_lines="3"
const handleCreateProjection = async (data) => {
    if (data) {
        await createTH1Projection(data.data.intersectionAxis, data.data, ['projection'], ['X', 'Y']) // create JSROOT histogram projection 
    }
}
```

As a final step we can ensure displaying a projection in panel in VR scene as well.
For this we only need to import another function from **react-ndmspc-core** `displayImageOfProjection` where we define correct parameters (source panel id, target panel ids and resolution).

Here is implemented function with possibility of displaying the projection on the panel in VR

```javascript title="ProjectionComponent.jsx" hl_lines="4"
const handleCreateProjection = async (data) => {
    if (data) {
        await createTH1Projection(data.data.intersectionAxis, data.data, ['projection'], ['X', 'Y']) // create JSROOT histogram projection 
        await displayImageOfProjection('projection', ['panel_0'], '800px', '800px')                  // set current projection view on the panel in VR
    }
}
```

!!! warning
    For the correct execution of the function we need to use `async` for the maintain function and `await` for both used functions.

!!! warning
    For the correct execution of the function we need to use `async` for the maintain function and `await` for both used functions.
    **Projection** is **id** of the source element
    **panel_0** is default **id** of the panel in `NdmVrScene`

![](images/projectionOnX.png "Application with displayed projection in web and VR on intersect side of the bin, that is corresponded with axis X. Here we have results distributed row on the Y axis for each bin on axis X")

![](images/projectionOnY.png "Application with displayed projection in web and VR on intersect side of the bin, that is corresponded with axis Y, Here we have results distributed row on the X axis for each bin on axis Y")

!!! note
    [Project state](https://gitlab.com/ndmspc/tutorials/ndmspc-developer/-/tree/53fc1bd025ae8b2b192dadd209a137c742821a7a)

## Implement displaying the count of the selected bins

At the end of this tutorial, we implement feature for displaying the selection count in `BinInfoComponent`.
For this, we don't need to add a new distributor and other functionality in `AppGlobalContext` because this we already have done. 
We ensured this distribution of the selected bins in whole application in previous parts.
We only need to receive this state variable in our component `BinInfoComponent` and display it.

We have to extend component `BinInfoComponent` with functionality:

- we add new state variable in this component
- we append the line for displaying content (in JSX return clause) and the bind the value of the state
- we create the external subscription with implemented handler function on the distributor with name **binSelect**

```javascript title="Bininfo.jsx"
// ... imports

const BinInfo = () => {
    // ... other states
    const [selectedCount, setSelectedCount] = useState(0)
    
    // ... other logic

    useEffect(() => {
        const subscription = appGlobalScope.createExternalSubscription(
            'binSelect',
            // receive all selected bins from main context and set it as a state 
            (data) => { setSelectedCount(data.length) },
            null
        )
        return () => subscription.unsubscribe()
    }, [])
    
    return (
        /* ... other elements */
        <p className="line"><b>selectedBinCount:</b>{selectedCount ? selectedCount : 0}</p>
    )
}

export default BinInfo
```

![](images/binSelectionCounting.png "Application with displayed selected bin count in BinInfoComponent in the aside panel")

!!! note
    [Project state](https://gitlab.com/ndmspc/tutorials/ndmspc-developer/-/tree/1e8f720ee289f0a1e17b7f976cc6c35659cc3823)














